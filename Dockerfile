FROM ubuntu:devel

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    tzdata \
    locales \
    xmonad \
    libghc-xmonad-extras-dev \
    # For startx
    xinit \
    # For xrdb
    x11-xserver-utils \
    # For Xorg.wrap
    xserver-xorg-legacy \
    xserver-xorg-input-libinput \
    brightnessctl \
    rxvt-unicode-256color \
    sudo \
    vim \
    software-properties-common \
    git \
    openssh-client \
    libavcodec-extra \
 && rm -rf /var/lib/apt/lists/*

RUN add-apt-repository -y ppa:willat8/ubuntu-desktop \
 && add-apt-repository -y ppa:willat8/unix-runescape-client \
 && apt-get install -y --no-install-recommends \
    xmobar \
    firefox \
    unix-runescape-client \
    # For RSU-Launcher
    libio-socket-ssl-perl \
 && rm -rf /var/lib/apt/lists/* # redo14

COPY group passwd shadow asound.conf /etc/
COPY keyboard /etc/default

RUN sed -i 's/console/anybody/' /etc/X11/Xwrapper.config \
 && echo "needs_root_rights=yes" >> /etc/X11/Xwrapper.config \
 && echo "will ALL = (root) NOPASSWD: /lib/systemd/systemd-udevd -d, /usr/bin/udevadm trigger" | EDITOR="tee -a" visudo \
 && chmod 640 /etc/shadow

# Prevent urxvtd from spawning a second process
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=733323
RUN chgrp root /usr/bin/urxvtd \
 && chmod g-s /usr/bin/urxvtd

RUN chmod 4711 /usr/bin/brightnessctl

RUN ln -sfv /usr/share/zoneinfo/Australia/Sydney /etc/localtime \
 && locale-gen en_AU.UTF-8
USER will
# Substitute for invoking /bin/login
ENV SHELL=/bin/bash \
    PATH="${PATH}:/usr/games" \
    LANG=en_AU.UTF-8 \
    _JAVA_AWT_WM_NONREPARENTING=1 \
    HISTSIZE= HISTFILESIZE=
WORKDIR /home/will

ENTRYPOINT sudo /lib/systemd/systemd-udevd -d && sudo udevadm trigger && exec startx

